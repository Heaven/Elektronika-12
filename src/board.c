#include "main.h"

/* private defines */
#define SPI_BUFFER_SIZE   (uint16_t)(24*8)

/* private macros */
#define HT1632C_CS_ON     GPIOA->BRR = (1<<15)
#define HT1632C_CS_OFF    GPIOA->BSRR = (1<<15)

/* private variables */
static uint8_t display_Buffer[SPI_BUFFER_SIZE] = {0};

/* private typedef */
/* private functions */
static void GPIO_Init(void);
static void I2C1_Init(void);
static void SPI1_Init(void);
//static void TIM1_Init(void);
//static void TIM3_Init(void);
//static void TIM14_Init(void);
//static void TIM16_Init(void);
//static void TIM17_Init(void);
//static void USART1_UART_Init(void);

/* Board perephireal Configuration  */
void Board_Init(void)
{
  /* At this stage the microcontroller clock setting is already configured, 
     this is done through SystemInit() function which is called from startup
     file (startup_stm32f072xb.s) before to branch to application main.
     To reconfigure the default setting of SystemInit() function, refer to
     system_stm32f0xx.c file
  */

  /* Main peripheral clock enable */
  RCC->AHBENR |= (RCC_AHBENR_GPIOAEN | RCC_AHBENR_GPIOBEN | RCC_AHBENR_DMAEN);
  RCC->APB1ENR = (RCC_APB1ENR_PWREN | RCC_APB1ENR_I2C1EN); // | RCC_APB1ENR_TIM14EN| RCC_APB1ENR_TIM3EN);
  RCC->APB2ENR = (RCC_APB2ENR_SYSCFGEN | RCC_APB2ENR_SPI1EN); // | RCC_APB2ENR_TIM1EN | RCC_APB2ENR_TIM16EN | RCC_APB2ENR_TIM17EN);

  /* Peripheral interrupt init*/
  /* RCC_IRQn interrupt configuration */
  NVIC_SetPriority(RCC_IRQn, 0);
  NVIC_EnableIRQ(RCC_IRQn);

  /* Initialize all configured peripherals */
  GPIO_Init();

  /* DMA interrupt init */
  /* DMA1_Channel2_3_IRQn interrupt configuration */
  NVIC_SetPriority(DMA1_Channel2_3_IRQn, 0);
  NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);

  I2C1_Init();

  SPI1_Init();
  /** Star SPI transfer to shift registers */
  /* Set DMA source and destination addresses. */
  /* Source: Address of the SPI buffer. */
  DMA1_Channel3->CMAR = (uint32_t)&display_Buffer;
  /* Destination: SPI1 data register. */
  DMA1_Channel3->CPAR = (uint32_t)&(SPI1->DR);
  /* Set DMA data transfer length (SPI buffer length). */
  DMA1_Channel3->CNDTR = SPI_BUFFER_SIZE;
  /* Enable SPI transfer */
  //SPI1->CR1 |= SPI_CR1_SPE;
//  Flag.SPI_TX_End = 1;
  //GPIOA->BRR = (1<<15); // set ~CS low

//  TIM1_Init();
//  TIM3_Init();
//  TIM14_Init();
//  TIM16_Init();
//  TIM17_Init();
//  USART1_UART_Init();
}

/* output 'L', 'G', '5' */
void display_test(void) {
  // prepare buffer
  display_Buffer[1] = 0xfe;
  display_Buffer[2] = 0x02;
  display_Buffer[3] = 0x02;
  display_Buffer[4] = 0x02;
  display_Buffer[5] = 0x02;

  display_Buffer[9] = 0x7c;
  display_Buffer[10] = 0x82;
  display_Buffer[11] = 0x92;
  display_Buffer[12] = 0x92;
  display_Buffer[13] = 0x5e;

  display_Buffer[16] = 0xf4;
  display_Buffer[17] = 0x92;
  display_Buffer[18] = 0x92;
  display_Buffer[19] = 0x92;
  display_Buffer[20] = 0x8c;

  // init ht1632
  // disable spi
  SPI1->CR1 &= ~(SPI_CR1_SPE);
  // disable dma tranfer & clear data width bits
  SPI1->CR2 &= ~(SPI_CR2_TXDMAEN | SPI_CR2_DS);
  // set data width to 12 bit for command mode
  SPI1->CR2 |= (SPI_CR2_DS_3 | SPI_CR2_DS_1 | SPI_CR2_DS_0);
  // enable spi
  SPI1->CR1 |= SPI_CR1_SPE;
  // select chip
  HT1632C_CS_ON;
  // wite for spi
  while ((SPI1->SR & SPI_SR_BSY) != 0);
  // transfer command
  SPI1->DR = 0x802; // 100 0000 0001 0 -- SYS_EN
  // wite for spi
  while ((SPI1->SR & SPI_SR_TXE) == 0);
  SPI1->DR = 0x806; // 100 0000 0011 0 -- LED_ON
  // wite for spi
  while ((SPI1->SR & SPI_SR_BSY) != 0);
  // deselect chip
  HT1632C_CS_OFF;
  // disable spi
  SPI1->CR1 &= ~(SPI_CR1_SPE);

  // set 10 bit data width for write cmd
  SPI1->CR2 &= ~(SPI_CR2_DS);
  SPI1->CR2 |= (SPI_CR2_DS_3 | SPI_CR2_DS_0);
  // enable spi
  SPI1->CR1 |= SPI_CR1_SPE;
  // select chip
  HT1632C_CS_ON;
  // wite for spi
  while ((SPI1->SR & SPI_SR_BSY) != 0);
  // transfer command
  SPI1->DR = 0x280; // 101 0000000 -- write from addr 0x0
  // wite for spi
  while ((SPI1->SR & SPI_SR_BSY) != 0);
  // deselect chip
  HT1632C_CS_OFF;
  // disable spi
  SPI1->CR1 &= ~(SPI_CR1_SPE);

  // set 8 bit data width for data
  SPI1->CR2 &= ~(SPI_CR2_DS);
  SPI1->CR2 |= (SPI_CR2_DS_2 | SPI_CR2_DS_1 | SPI_CR2_DS_0);
  // enable spi
  SPI1->CR1 |= SPI_CR1_SPE;
  // select chip
  HT1632C_CS_ON;
  // start transfer
  SPI1->CR2 |= (SPI_CR2_TXDMAEN);
  DMA1_Channel3->CCR |= DMA_CCR_EN;
}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void GPIO_Init(void)
{
  /* EXTI Line: falling, pull-up, input */
  SYSCFG->EXTICR[3] = 0;
  /* Enable IT on provided Lines */
  EXTI->IMR |= EXTI_IMR_IM12;
  /* Enable Falling Trigger on provided Lines */
  EXTI->FTSR |= EXTI_IMR_IM12;
  /* EXTI interrupt init*/
  NVIC_SetPriority(EXTI4_15_IRQn, 0);
  NVIC_EnableIRQ(EXTI4_15_IRQn);

  /* Select output mode (01) PP+PU, High Speed
   PA8  - Buzzer (AF2 for TIM1_CH1)
   PA15 - ~CS / SPI_NSS (AF0)
   PB4  - ~RD / SPI_MISO (AF0) - NOT USED
   * Select output mode (10) AF+OD+PU, High Speed
   PB3  - ~WR / SPI_SCK (AF0)
   PB5  - Data / SPI_MOSI (AF0)
   PB6  - SCL (AF1)
   PB7  - SDA (AF1)
  */
  // MODE Output
  GPIOA->MODER = (GPIOA->MODER & ~(GPIO_MODER_MODER8 | GPIO_MODER_MODER15)) \
                | (GPIO_MODER_MODER8_0 | GPIO_MODER_MODER15_0);
  GPIOB->MODER = (GPIOB->MODER & ~(GPIO_MODER_MODER3|GPIO_MODER_MODER5|GPIO_MODER_MODER6|GPIO_MODER_MODER7)) \
                | (GPIO_MODER_MODER3_1|GPIO_MODER_MODER5_1|GPIO_MODER_MODER6_1|GPIO_MODER_MODER7_1);
  // Pull-Up
  GPIOA->PUPDR = (GPIOA->PUPDR & ~(GPIO_PUPDR_PUPDR8 | GPIO_PUPDR_PUPDR15)) \
                | (GPIO_PUPDR_PUPDR8_0 | GPIO_PUPDR_PUPDR15_0);
  /*GPIOB->PUPDR = (GPIOB->PUPDR & ~(GPIO_PUPDR_PUPDR3|GPIO_PUPDR_PUPDR5|GPIO_PUPDR_PUPDR6|GPIO_PUPDR_PUPDR7)) \
                | (GPIO_PUPDR_PUPDR3_0|GPIO_PUPDR_PUPDR5_0|GPIO_PUPDR_PUPDR6_0|GPIO_PUPDR_PUPDR7_0);*/
  // High Speed
  GPIOA->OSPEEDR = (GPIO_OSPEEDR_OSPEEDR8|GPIO_OSPEEDR_OSPEEDR15);
  GPIOB->OSPEEDR = (GPIO_OSPEEDR_OSPEEDR3|GPIO_OSPEEDR_OSPEEDR5 \
                  |GPIO_OSPEEDR_OSPEEDR6|GPIO_OSPEEDR_OSPEEDR7);
  // Open Drain
  GPIOB->OTYPER = (GPIO_OTYPER_OT_3|GPIO_OTYPER_OT_5 \
                  |GPIO_OTYPER_OT_6|GPIO_OTYPER_OT_7);
  // AF1 for PB6 & PB7
  GPIOB->AFR[0] = (0x1<<24) | (0x1<<28);

  /* Select Pull-Up for input pins
   PA0 - BTN1 / H+
   PA1 - BTN2 / M+
   PA2 - SW1 / Stop
   PA3 - SW2 / Sec
   PA4 - BTN3 / Res
   PA5 - SW3 / AlarmSet
   PA6 - SW5 / AlarmOff
   PA7 - SW4 / Bright
   PA12 - RTC IRQ / Exti
  */
  GPIOA->PUPDR |= (GPIO_PUPDR_PUPDR0_0|GPIO_PUPDR_PUPDR1_0|GPIO_PUPDR_PUPDR2_0 \
                |GPIO_PUPDR_PUPDR3_0|GPIO_PUPDR_PUPDR4_0|GPIO_PUPDR_PUPDR5_0 \
                |GPIO_PUPDR_PUPDR6_0|GPIO_PUPDR_PUPDR7_0|GPIO_PUPDR_PUPDR12_0);
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void I2C1_Init(void)
{
  /* I2C1 interrupt Init */
  NVIC_SetPriority(I2C1_IRQn, 0);
  NVIC_EnableIRQ(I2C1_IRQn);

  /** I2C Initialization: I2C_Fast */
  I2C1->CR1 = 0x0;
  I2C1->CR2 = 0x0;
  I2C1->TIMINGR = 0x2010091A; // 0x00901850 ? 
  I2C1->CR1 = I2C_CR1_PE;
  I2C1->CR2 = I2C_CR2_AUTOEND;
}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void SPI1_Init(void)
{
  /* SPI1 DMA Init */
  /* SPI1_TX Init: Priority high, Memory increment, read from memory, non-circular mode,
     ?Enable DMA transfer complete/error interrupts */
  DMA1_Channel3->CCR = (DMA_CCR_PL_1 | DMA_CCR_MINC | DMA_CCR_DIR); // DMA_CCR_CIRC | DMA_CCR_TEIE | DMA_CCR_DIR | DMA_CCR_TCIE

  /* SPI1 interrupt Init */
  NVIC_SetPriority(SPI1_IRQn, 0);
  NVIC_EnableIRQ(SPI1_IRQn);

  /* SPI1 parameter configuration: master mode, data 8 bit, divider = 64, TX DMA */
  // SPI_CR1_CPOL ?
  SPI1->CR1 = (SPI_CR1_MSTR | SPI_CR1_BR_2 | SPI_CR1_BR_0 | SPI_CR1_SSM | SPI_CR1_SSI); // SPI_CR1_BIDIMODE | SPI_CR1_BIDIOE | 
  SPI1->CR2 = (SPI_CR2_DS_2 | SPI_CR2_DS_1 | SPI_CR2_DS_0 | SPI_CR2_TXDMAEN); // | SPI_CR2_FRXTH);
}

/**
  * @brief This function handles EXTI line 4 to 15 interrupts.
  */
void EXTI4_15_IRQHandler(void)
{
  if ((EXTI->PR & EXTI_IMR_IM12) != 0)
  {
    EXTI->PR = EXTI_IMR_IM12;
    Flag.RTC_IRQ = 1;
    //ES_PlaceEvent(evNewSecond);
  }
}
