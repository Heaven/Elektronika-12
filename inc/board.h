#pragma once
#ifndef _BOARD_H_
#define _BOARD_H_

/* Includes */
#include "main.h"

/* Type Defs */

/* Exported macros */
/* Constants */
/* Defines */
/* Variables */
/* Exported funcions */
void Board_Init(void);
void EXTI4_15_IRQHandler(void);
void display_test(void);

#endif /* _BOARD_H_ */
