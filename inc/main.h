/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _MAIN_H_
#define _MAIN_H_

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdlib.h>
#include "stm32f0xx.h"

/* Private includes ----------------------------------------------------------*/
#include "board.h"
#include "rtos.h"
//#include "i2c.h"
//#include "ds3231.h"
//#include "event-system.h"
//#include "list_event.h"
//#include "clock.h"

/* Exported types ------------------------------------------------------------*/
typedef enum {
  DOWN = 0,
  UP = 1
} updown_t;

typedef enum {
  OFF = 0,
  ON = 1
} onoff_t;

typedef struct t_flag {
  uint32_t RTC_IRQ:     1;
  uint32_t _reserv:    31;
} flag_t;
extern volatile flag_t Flag;

typedef union {
  uint16_t u16;           // element specifier for accessing whole u16
  int16_t  i16;           // element specifier for accessing whole i16
  uint8_t  u8[2];
  struct {
    #ifdef LITTLE_ENDIAN  // Byte-order is little endian
    uint8_t u8L;          // element specifier for accessing low u8
    uint8_t u8H;          // element specifier for accessing high u8
    #else                 // Byte-order is big endian
    uint8_t u8H;          // element specifier for accessing low u8
    uint8_t u8L;          // element specifier for accessing high u8
    #endif
  } s16;                  // element spec. for acc. struct with low or high u8
} nt16_t;

typedef union {
  uint32_t  u32;          // element specifier for accessing whole u32
  int32_t   i32;          // element specifier for accessing whole i32
  uint16_t  u16[2];
  uint8_t   u8[4];
 struct {
    #ifdef LITTLE_ENDIAN  // Byte-order is little endian
    uint16_t u16L;        // element specifier for accessing low u16
    uint16_t u16H;        // element specifier for accessing high u16
    #else                 // Byte-order is big endian
    uint16_t u16H;        // element specifier for accessing low u16
    uint16_t u16L;        // element specifier for accessing high u16
    #endif
  } s32;                  // element spec. for acc. struct with low or high u16
} nt32_t;

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* Private defines -----------------------------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* _MAIN_H_ */
