/******************************************************************************************
 * Based on the task scheduler from the site ChipEnable.ru                                *
 * http://chipenable.ru/index.php/programming-avr/item/110-planirovschik.html             *
 *                                                                                        *
 * Modified Shibanov Vladimir aka KontAr                                                  *
 * Date: 26.03.2014                                                                       *
 *                                                                                        *
 * Changes:                                                                               *
 * - added single task call                                                               *
 * - added delete task by name                                                            *
 * - when a task is added again, its variables are updated                                *
 * - added a pointer to the "tail" of the list                                            *
 * - RTOS functions adjusted for the "tail"                                               *
 ******************************************************************************************
 * shilov, 2015.04.07                                                                     *
 * combined with the module of millisecond delays on the timer                            *
 ******************************************************************************************/
#pragma once
#ifndef RTOS_H
#define RTOS_H

#include "stm32f0xx.h"

/**
 * @brief Number of tasks
 */
#define MAX_TASKS       20

/**
 * Function Prototypes
 */
void RTOS_Init (void);
void RTOS_SetTask (void (*taskFunc)(void), uint32_t taskDelay, uint32_t taskPeriod);
void RTOS_DeleteTask (void (*taskFunc)(void));
void RTOS_DispatchTask (void);
void RTOS_Timer (void);
void SysTick_Handler(void);
void tdelay_ms(uint32_t msek);

#endif
